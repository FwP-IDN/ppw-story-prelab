from django.contrib import admin
from .models import PersonalSchedule, Backdoor

# Register your models here.
admin.site.register(PersonalSchedule)
admin.site.register(Backdoor)
