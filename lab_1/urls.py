from django.urls import re_path
from .views import *
# url for app
urlpatterns = [
    re_path(r'^$', index, name='index'),
    re_path(r'^home/$', home, name='home'),
    re_path(r'^register/$', register, name='register'),
    re_path(r'^education/$', education, name='education'),
    re_path(r'^experience/$', experience, name='experience'),
    re_path(r'^skill/$', skill, name='skill'),
    re_path(r'^project/$', project, name='project'),
    re_path(r'^schedule/$', schedule, name='schedule'),
    re_path(r'^schedule/deleteAll$', schedule_delete_all, name='delete_all_schedule'),
    re_path(r'^backdoor/$', backdoor, name='backdoor'),
]
