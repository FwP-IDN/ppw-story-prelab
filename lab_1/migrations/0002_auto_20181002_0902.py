# Generated by Django 2.1.1 on 2018-10-02 09:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lab_1', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='PersonalSchedule',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('date', models.DateField()),
                ('time', models.TimeField()),
                ('event', models.CharField(max_length=100)),
                ('place', models.CharField(max_length=100)),
            ],
        ),
        migrations.DeleteModel(
            name='JadwalPribadi',
        ),
    ]
