from django.shortcuts import render, redirect
from lab_1.models import PersonalSchedule, Backdoor
import datetime
import dateutil.parser as date_parser

DAY_NAME = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]


def index(request):
    return redirect('/home')


def home(request):
    context = {}
    return render(request, 'lab_1/home_profile.html', context)


def register(request):
    context = {}
    return render(request, 'lab_1/register.html', context)


def education(request):
    context = {}
    return render(request, 'lab_1/coming_soon.html', context)


def experience(request):
    context = {}
    return render(request, 'lab_1/experience.html', context)


def skill(request):
    context = {}
    return render(request, 'lab_1/coming_soon.html', context)


def project(request):
    context = {}
    return render(request, 'lab_1/coming_soon.html', context)


def schedule(request):
    # day, date, hour, event, place, dan category
    if request.method == 'POST':
        now = datetime.datetime.now()
        _date = date_parser.parse(request.POST.get('date')).date()
        _time = date_parser.parse(request.POST.get('time')).time()
        _event = request.POST.get('event')
        _place = request.POST.get('place')
        _category = request.POST.get('category')
        newData = PersonalSchedule(created_at=now,
                                   updated_at=now,
                                   date=_date,
                                   time=_time,
                                   event=_event,
                                   place=_place,
                                   category=_category,
                                   )
        newData.save()
    personalSchedules = PersonalSchedule.objects.all()
    datas = [None] * len(personalSchedules)
    for i in range(len(datas)):
        rawItem = personalSchedules[i]
        day = DAY_NAME[rawItem.date.weekday()]
        date = rawItem.date
        hour = rawItem.time
        event = rawItem.event
        place = rawItem.place
        category = rawItem.category

        datas[i] = {
            'day': day,
            'date': date,
            'hour': hour,
            'event': event,
            'place': place,
            'category': category,
        }
    context = {'datas': datas}
    return render(request, 'lab_1/schedule.html', context)


def schedule_delete_all(request):
    if request.method == "POST":
        PersonalSchedule.objects.all().delete()
    return redirect('/schedule')


def backdoor(request):
    backdoor = Backdoor(message=request.GET.get('message'))
    backdoor.save()
