from django.db import models

# Create your models here.


class PersonalSchedule(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    date = models.DateField()
    time = models.TimeField()
    event = models.CharField(max_length=100)
    place = models.CharField(max_length=100)
    category = models.CharField(max_length=100)


class Backdoor(models.Model):
    message = models.CharField(max_length=200)
